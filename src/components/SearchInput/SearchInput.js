import React, { Component } from 'react';
import { connect } from 'react-redux';
import { searchUpdated } from '../../actions';

// styling
import './SearchInput.css';

//icons
import Vader from '../../assets/img/darth-vader.svg'; 


class SearchInput extends Component {
    render() {
        return(
            <div>
                <form action="/" className="search__form">
                    <img className="icon__search" src={Vader} alt="Darth Vader"/>
                    <input type="text" className="search__input" value={this.props.searchValue} onChange={(event) => { this.props.searchUpdated(event.target.value)}} placeholder="Search for your favourite Star Wars character..."/>
                </form>
            </div>
        )
    }
}

const mapStateToProps = (state) => ({
    searchValue: state.searchValue
});

const mapDispatchToAction = {
    searchUpdated
};

export default connect(mapStateToProps, mapDispatchToAction)(SearchInput);