import React, { Component } from 'react';
import { connect } from 'react-redux';
import { resultClicked } from '../../actions';

// styling
import './CatalogSingle.css';

//icons
import Birthday from '../../assets/img/birthday.svg';
import Man from '../../assets/img/man.svg';
import Open from '../../assets/img/plus.svg';
import Genders from '../../assets/img/genders.svg';


class CatalogSingle extends Component {
     render() {

        let height;
        if(this.props.height === 'unknown') {
            height = <p className="character__text">{this.props.height}</p>
        } else {
            height = <p className="character__text">{`${this.props.height}cm`}</p>
        }

        return(
            <div className="catalog__single" onClick={() => { this.props.resultClicked(this.props.name)}}>
                <p className="character__name">{this.props.name}</p>
                <div className="character__stats">
                    <div className="character__stat">
                        <img src={Birthday} className="icon__birthday" alt="Birthday Icon" />
                        <p className="character__text">{this.props.birth_year}</p>                    
                    </div>
                    <div className="character__stat">
                        <img src={Man} className="icon__height" alt="Person Icon" />
                        {height}                    
                    </div>
                    <div className="character__stat">
                        <img src={Genders} className="icon__genders" alt="Gender Icon" />
                        <p className="character__text">{this.props.gender}</p>                    
                    </div>
                </div>
                <img src={Open} className="icon__open" alt="Open Icon" />    
            </div>
        )
    }
}

const mapStateToProps = (state) => ({
    modalOpen: state.modalOpen
});

const mapDispatchToAction =  {
    resultClicked
};

export default connect(mapStateToProps, mapDispatchToAction)(CatalogSingle);