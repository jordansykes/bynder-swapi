import React,{ Component } from 'react';
import { connect } from 'react-redux';
import { resultClicked } from '../../actions';

//icons
import Group from '../../assets/img/group.svg';


class ExtraPeople extends Component {
    renderResidents = (resident) => {
        return <li className="resident__single" onClick={() => { this.props.resultClicked(resident.name)}} key = {resident.name}>{resident.name}</li>
    }
    
    render() {
        const residentsList = this.props.residents.map(this.renderResidents);
        if(residentsList.length){
            return(
                <div className="modal__stat--lrg">                                            
                    <img src={Group} className="icon__group--xlrg" alt="Group Icon" />                                                                                
                    <p className="group__text">Other people from this planet</p>
                    <ul className="modal__people-planet">
                        {residentsList}
                    </ul>
                </div>
            )
        }
        return null;
    }
}


const mapStateToProps = (state, props) => {
    const residents = props.planetResidents.map((resident) => {
        return state.characters.find((character) => (
            character.url === resident
        ));
    });
    const residentNotIncludingSelf = residents.filter((resident) => resident.name !== state.currentCharacter)
    return {
        residents: residentNotIncludingSelf
    }
};

const mapDispatchToAction = {
    resultClicked
};

export default connect(mapStateToProps, mapDispatchToAction)(ExtraPeople);
