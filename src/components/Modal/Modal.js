import React, { Component } from 'react';
import { connect } from 'react-redux';
import { modalClosed } from '../../actions';
import ExtraPeople from './ExtraPeople';

// styling
import './Modal.css';

// icons
import Close from '../../assets/img/close.svg';
import Birthday from '../../assets/img/birthday.svg';
import Man from '../../assets/img/man.svg';
import Genders from '../../assets/img/genders.svg';
import Planet from '../../assets/img/planet.svg';

class Modal extends Component {
    
    state = {
        modalData: null
    }

    componentWillReceiveProps(newProps) {
        // fetching the planet that the character is from, from the API
        if(newProps.characterData) { 
            const { homeworld } = newProps.characterData;
            fetch(homeworld)
            .then(res => res.json())
            .then(json => {
                this.setState({
                    modalData: json  
                })
            })
        }
    }

    render() {
        if (this.props.modalOpen === false || this.state.modalData === null) {
            return null;
        }

        let height;
        if(this.props.characterData.height === 'unknown') {
            height = <p>{this.props.characterData.height}</p>
        } else {
            height = <p>{`${this.props.characterData.height}cm`}</p>
        }
        
        return(
            <div className="modal__wrapper">
                <div className="modal__background" onClick={() => { this.props.modalClosed()}}></div> 
                <div className="modal__content">
                    <img src={Close} className="icon__close" onClick={() => { this.props.modalClosed()}} alt="Close Icon" />
                    <div className="modal__character">
                        <img src="https://placeholdit.co//i/150x150" className="placeholder" alt="Character"/>
                    </div>
                    <h1 className="modal__name">{this.props.characterData.name}</h1>

                    <div className="modal__stats">
                        <div className="modal__stat">
                            <img src={Birthday} className="icon__birthday--lrg" alt="Birthday Icon" />
                            <p>{this.props.characterData.birth_year}</p>
                        </div>
                        <div className="modal__stat">                    
                            <img src={Genders} className="icon__genders--lrg" alt="Gender Icon" />
                            <p>{this.props.characterData.gender}</p>
                        </div>
                        <div className="modal__stat">                    
                            <img src={Man} className="icon__height--lrg" alt="Man Icon" />                                        
                            {height}    
                        </div>
                        <div className="modal__stat">                    
                            <img src={Planet} className="icon__planet--lrg" alt="Planet Icon" />                                                            
                            <p>{this.state.modalData.name}</p>
                        </div>

                        <ExtraPeople planetResidents = {this.state.modalData.residents} key = {this.props.name}/>

                    </div>  

                </div>
            </div>
        ); 
     }
}

const mapStateToProps = (state) => {
    const {currentCharacter, characters} = state;
    const characterData = (characters.find((character) => character.name === currentCharacter))

    return {
        modalOpen: state.modalOpen,
        characterData
    }
};

const mapDispatchToAction = {
    modalClosed  
};

export default connect(mapStateToProps, mapDispatchToAction)(Modal);