import React, { Component } from 'react';
import FilterSingle from '../FilterSingle/FilterSingle';
import FilterSort from '../FilterSort/FilterSort';

// styling
import './FiltersWrapper.css';

export default class FilterWrapper extends Component {
     render() {
        return(
            <div className="filters__wrapper">
                <FilterSingle />
                <FilterSort />
            </div>
        );
     }
}