import React, { Component } from 'react';
import { connect } from 'react-redux';
import { sortChanged } from '../../actions';

// styling
import './FilterSort.css';


class FilterSort extends Component {
     render() {
        return(
            <div className="filter__sort">
                <select name="" id="" className="select__dropdown" value={this.props.order} onChange={(event) => { this.props.sortChanged(event.target.value)}}>
                    <option value="alphabetically">Alphabetically</option>
                    <option value="height">Shortest to Tallest</option>
                </select>
            </div>
        );
     }
}

const mapStateToProps = (state) => ({
    order: state.order
});

const mapDispatchToAction = {
    sortChanged    
};

export default connect(mapStateToProps, mapDispatchToAction)(FilterSort);
