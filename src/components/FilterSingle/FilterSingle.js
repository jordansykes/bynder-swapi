import React, { Component } from 'react';
import { connect } from 'react-redux';
import { filterResults } from '../../actions';

// styling
import './FilterSingle.css';

class FilterSingle extends Component {
     render() {
        return(
            <div className="filter__singles">
                <div className="filter__single">
                    <label>
                        <input type="radio" name="filterBy" value="male" onChange = {(event) => { this.props.filterResults(event.target.value)}} checked={this.props.filteredBy === "male"}/> Male
                    </label>
                </div>
                <div className="filter__single">
                    <label>
                        <input type="radio" name="filterBy" value="female" onChange = {(event) => { this.props.filterResults(event.target.value)}} checked={this.props.filteredBy === "female"}/> Female
                    </label>
                </div>
                <div className="filter__single">
                    <label>
                        <input type="radio" name="filterBy" value="n/a" onChange = {(event) => { this.props.filterResults(event.target.value)}} checked={this.props.filteredBy === "n/a"}/> Other
                    </label>
                </div>
                <button className="clear__filters" onClick = {(event) => { this.props.filterResults("")}}>Clear Filters</button>
            </div>
        );
     }
}


const mapStateToProps = (state) => ({ 
    filteredBy: state.filteredBy
});

const mapDispatchToAction = {
    filterResults
};

export default connect(mapStateToProps, mapDispatchToAction)(FilterSingle);