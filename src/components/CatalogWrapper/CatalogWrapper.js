import React, { Component } from 'react';
import { connect } from 'react-redux';
import CatalogSingle from '../CatalogSingle/CatalogSingle';
// import { InitialData } from '../../reducers';
import { getAllCharacters } from '../../reducers';

// styling
import './CatalogWrapper.css';

class CatalogWrapper extends Component {
    
    componentWillMount() {
        this.props.getAllCharacters();
    }

    render() {
        let searchValue = this.props.searchValue;
        let characters = this.props.characters;
        let filteredBy = this.props.filteredBy;
        let charactersList = [];
        let order = this.props.order;
        let loadingMessage;


        if(typeof characters !== 'undefined') {
            
            // sort characters alphabetically - default view
            characters.sort((a, b) => {
                let ab = a.name.toLowerCase();
                let bc = b.name.toLowerCase();
                if (ab < bc) {
                    return -1;
                }
                if (ab > bc) {
                    return 1;
                }
                return 0;
            });

            //sort characters from shortest to tallest
            if(order === 'height') {
                characters.sort((a,b) => {
                    return a.height - b.height
                });
            }

            // if there is a search value, filter the list of results to
            // only include what matches the text string
            if(searchValue) {
                characters = characters.filter(character =>
                character.name.toLowerCase()
                .includes(searchValue.toLowerCase()));
            }

            // if there a filter radio button is checked, filter the results
            // to the value of filteredBy in state.
            if(filteredBy) {
                characters = characters.filter(character =>
                character.gender === filteredBy);
            }

            // map over the characters in the characterslist
            charactersList = characters.map((character, index) => {
                return(
                    <CatalogSingle {...character} key = {index} index = {index}/>
                )
            });
        }

        // a loading state whilst the data is being requested from the API
        if(this.props.searchValue.length > 1 && characters.length < 1) {
            loadingMessage = <div className="loading__feedback">No results, try again...</div>
        } else if(characters.length < 1) {
            loadingMessage = <div className="loading__feedback">Programming the flux capacitor...</div> 
        }

        return(
            <div className="catalog__wrapper">
                {loadingMessage}
                {charactersList}
            </div>
        );
     }
}

const mapStateToProps = (state) => ({
    characters: state.characters,
    searchValue: state.searchValue,    
    order: state.order,
    filteredBy: state.filteredBy
});

const mapDispatchToAction = {
    getAllCharacters
};

export default connect(mapStateToProps, mapDispatchToAction)(CatalogWrapper);