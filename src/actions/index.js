export const FETCH_CHARACTERS = 'FETCH_CHARACTERS';
export const API_ERROR = 'API_ERROR';
export const SEARCH_UPDATED = 'SEARCH_UPDATED';
export const RESULT_CLICKED = 'RESULT_CLICKED';
export const MODAL_CLOSED = 'MODAL_CLOSED';
export const SORT_CHANGED = 'SORT_CHANGED';
export const FILTER_RESULTS = 'FILTER_RESULTS';


// action creators
export const fetchCharacters = (results) => ({
    type: FETCH_CHARACTERS,
    payload: {
        results
    }
});

export const apiError = (err) => ({
    type: API_ERROR,
    payload: err
});

export const searchUpdated = (searchValue) => ({
    type: SEARCH_UPDATED,
    payload: searchValue
});

export const resultClicked = (name) =>  ({
    type: RESULT_CLICKED,
    payload: {
        modalOpen: true,
        name
    }
});

export const modalClosed = () =>  ({
    type: MODAL_CLOSED,
    payload: {
        modalOpen: false
    }
});

export const sortChanged = (sortBy) =>  ({
    type: SORT_CHANGED,
    payload: sortBy
});

export const filterResults = (filterBy) =>  ({
    type: FILTER_RESULTS,
    payload: filterBy
});