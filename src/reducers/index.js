import { FETCH_CHARACTERS, SEARCH_UPDATED, RESULT_CLICKED, MODAL_CLOSED, SORT_CHANGED,FILTER_RESULTS} from '../actions';
import { fetchCharacters, apiError } from '../actions';


const defaultState = {
    characters:[],
    modalOpen:false,
    searchValue:"",
    order:"alphabetically",
    filteredBy:""
}

// reducer
export default function reducer(state = defaultState, action){
    switch(action.type) {
        case FETCH_CHARACTERS: {
             return Object.assign({}, state, {
                 characters: action.payload.results
             });
        }
 
        case SEARCH_UPDATED: {
             return Object.assign({}, state, {
                searchValue: action.payload
            });
        }
 
        case RESULT_CLICKED: {
             return Object.assign({}, state, {
                modalOpen: action.payload.modalOpen,
                currentCharacter: action.payload.name
            });
        }
 
        case MODAL_CLOSED: {
             return Object.assign({}, state, {
                modalOpen: action.payload.modalOpen
            });
        }
 
        case SORT_CHANGED: {
             return Object.assign({}, state, {
                order: action.payload
            });
        }

        case FILTER_RESULTS: {
            return Object.assign({}, state, {
                filteredBy: action.payload
            });
       }
 
        default: {
            return state;
        }
    }
 }

  export function getAllCharacters() {
    return async (dispatch) => {
        try {
            let characters= [];
            let first  = await getCharacters(1);
            characters = [...first.results]      
            let pages = (Math.ceil(first.count / first.results.length));
            let promises = [];
    
            for (let i = 2; i <= pages; i++){
                promises.push(getCharacters(i))
            }
    
            let data = await Promise.all(promises);
            for(let response of data) {
                characters = characters.concat(response.results)
                dispatch(fetchCharacters(characters))
            }
        } catch (e) {
            //error
        }
    }
  };
  
  
  async function getCharacters(page) {
     const response = await fetch(`https://swapi.co/api/people/?page=${page}`);
     let data = await response.json();
     return data;
  }
 