import React, { Component } from 'react';
import SearchInput from './components/SearchInput/SearchInput';
import FiltersWrapper from './components/FiltersWrapper/FiltersWrapper';
import CatalogWrapper from './components/CatalogWrapper/CatalogWrapper';
import Modal from './components/Modal/Modal';

class App extends Component {
    render() {
        return (
            <div className="page__wrapper">
                <div className="app__wrapper">
                    <SearchInput />
                    <FiltersWrapper />
                    <CatalogWrapper />
                </div>
                <Modal/>
            </div>
        );
    }
}

export default App;
