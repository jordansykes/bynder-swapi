import React from 'react';
import ReactDOM from 'react-dom';
import thunk from 'redux-thunk';
import {createStore, applyMiddleware} from 'redux';
import {Provider} from 'react-redux';
import App from './App';
import registerServiceWorker from './registerServiceWorker';
import reducer from './reducers';
import {loadState, saveState} from './localStorage';

// styling
import './index.css';

/* eslint-disable no-underscore-dangle */

// attempt to get state from local storage
const persistedState = loadState();

const store = createStore(
    reducer,
    persistedState,
    applyMiddleware(thunk)
);

store.subscribe(() => {
    saveState(store.getState());
});


 /* eslint-enable */

 ReactDOM.render(
    <Provider store={store}>
        <App />
    </Provider>,
    document.getElementById('root')
);
registerServiceWorker();
